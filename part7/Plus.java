import java.util.*;
public class Plus {

	public static Seq plus(Constant a, Constant b)
	{
		ConstantIt c = new ConstantIt(a);
		ConstantIt d = new ConstantIt(b);
		int value = 0 ,len = 0;

		try{
		while(c.hasNext() && d.hasNext())
		{
			value = c.next() + d.next();
			len ++;
		}
		}catch (UsingIteratorPastEndException e){
			
		}
		return new Constant(len,value);
	}
	public static Seq plus(Constant a, Delta b)
	{
		ConstantIt c = new ConstantIt(a);
		DeltaIt d = new DeltaIt(b);
		int start = 0, len = 0, delta = 0;
		//boolean beginning = true;
		
		try{
		if(c.hasNext() && d.hasNext())
		{
			start = c.next() + d.next();
			len++;
		}
		if(c.hasNext() && d.hasNext())
		{
			delta = c.next() + d.next() - start;
			len++;
		}
		while(c.hasNext() && d.hasNext())
		{
			c.next();
			d.next();
			len ++;
		}
		}catch (UsingIteratorPastEndException e){
			System.err.println("SeqIt called past end");
			System.exit(1);
		}
		return new Delta(len,start,delta);
	}
	public static Seq plus(Delta a, Constant b)
	{
		DeltaIt d = new DeltaIt(a);
		ConstantIt c = new ConstantIt(b);
		
		int start = 0, len = 0, delta = 0;
		//boolean beginning = true;
		
		try{
		if(c.hasNext() && d.hasNext())
		{
			start = c.next() + d.next();
			len++;
		}
		if(c.hasNext() && d.hasNext())
		{
			delta = c.next() + d.next() - start;
			len++;
		}
		while(c.hasNext() && d.hasNext())
		{
			c.next();
			d.next();
			len ++;
		}
		}catch (UsingIteratorPastEndException e){
			System.err.println("SeqIt called past end");
			System.exit(1);
		}
		return new Delta(len,start,delta);
	}
	public static Seq plus(Delta a, Delta b)
	{
		DeltaIt c = new DeltaIt(a);
		DeltaIt d = new DeltaIt(b);
		int start = 0, len = 0, delta = 0;
		//boolean beginning = true;
		
		try{
		if(c.hasNext() && d.hasNext())
		{
			start = c.next() + d.next();
			len++;
		}
		if(c.hasNext() && d.hasNext())
		{
			delta = c.next() + d.next() - start;
			len++;
		}
		while(c.hasNext() && d.hasNext())
		{
			c.next();
			d.next();
			len ++;
		}
		}catch (UsingIteratorPastEndException e){
			System.err.println("SeqIt called past end");
			System.exit(1);
		}
		return new Delta(len,start,delta);
	}
	public static Seq plus(Jumble a, Jumble b)
	{
		JumbleIt c = new JumbleIt(a);
		JumbleIt d = new JumbleIt(b);
		int len = 0, count = 0;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
			while(c.hasNext() && d.hasNext())
			{
				c.next();
				d.next();
				len++;
			}
			c = new JumbleIt(a);
			d = new JumbleIt(b);
			int []result = new int[len];
			while(c.hasNext() && d.hasNext())
			{
				result[count]= c.next() + d.next();
				count++;
			}
			return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		return new Jumble(new int [1]);
	
	}
	public static Seq plus(Constant a, Jumble b)
	{
		ConstantIt c = new ConstantIt(a);
		JumbleIt d = new JumbleIt(b);
		int len = 0, count = 0;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
			while(c.hasNext() && d.hasNext())
			{
				c.next();
				d.next();
				len++;
			}
			c = new ConstantIt(a);
			d = new JumbleIt(b);
			int []result = new int[len];
			while(c.hasNext() && d.hasNext())
			{
				result[count]= c.next() + d.next();
				count++;
			}
			return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		
		return new Jumble(new int [1]);
	
	}
	public static Seq plus(Jumble a, Constant b)
	{
		ConstantIt c = new ConstantIt(b);
		JumbleIt d = new JumbleIt(a);
		int len = 0, count = 0;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
			while(c.hasNext() && d.hasNext())
			{
				c.next();
				d.next();
				len++;
			}
			c = new ConstantIt(b);
			d = new JumbleIt(a);
			int []result = new int[len];
			while(c.hasNext() && d.hasNext())
			{
				result[count]= c.next() + d.next();
				count++;
			}
			return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		return new Jumble(new int [1]);
		
	}
	public static Seq plus(Delta a, Jumble b)
	{
		DeltaIt c = new DeltaIt(a);
		JumbleIt d = new JumbleIt(b);
		int len = 0, count = 0;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
			while(c.hasNext() && d.hasNext())
			{
				c.next();
				d.next();
				len++;
			}
			c = new DeltaIt(a);
			d = new JumbleIt(b);
			int []result = new int[len];
			while(c.hasNext() && d.hasNext())
			{
				result[count]= c.next() + d.next();
				count++;
			}
			return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		return new Jumble(new int [1]);
	}
	public static Seq plus(Jumble a, Delta b)
	{
		DeltaIt c = new DeltaIt(b);
		JumbleIt d = new JumbleIt(a);
		int len = 0, count = 0;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
		while(c.hasNext() && d.hasNext())
		{
			c.next();
			d.next();
			len++;
		}
		c = new DeltaIt(b);
		d = new JumbleIt(a);
		int []result = new int[len];
		while(c.hasNext() && d.hasNext())
		{
			result[count]= c.next() + d.next();
			count++;
		}
		return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		return new Jumble(new int [1]);
	
	}
}
