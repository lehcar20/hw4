
public class JumbleUser {
	private static JumbleIt ji;
	public static int lengthLongestNDCSS1(Jumble j)
	{
		ji = new JumbleIt(j);
		int prev;
		int size = 0;
		int max = 0;
		int cur;
		try
		{
			if(ji.hasNext())
			{
				size++;
				prev = ji.next();
			}
			else
				return max;	//if it returns here size = 0 and max = 0
			
			while(ji.hasNext())
			{
				cur = ji.next();
				if(cur >= prev)
				{
					size++;
				}
				else
				{
					if(size > max)	//if the new size is the new max
						max = size;
					size = 1;	//resets size. goes to 1 because we include cur as new prev
				}
				prev = cur;
			}
		}//end try
		catch(UsingIteratorPastEndException e)
		{
			
		}
		if(max == 0 || size > max)	//if there is only one string of NDCSS
			max = size;
		return max;
		
	}
	
	public static int lengthLongestNDCSS2(Jumble j)
	{
		ji = new JumbleIt(j);
		int prev;
		int size = 0;
		int max = 0;
		int cur;
		try
		{
			prev = ji.next();
			size++;
		}
		catch(UsingIteratorPastEndException e) //if the size of array is zero
		{
			return max; //max would equal zero here
		}
		
		while(true)//endless loop until exception called
		{
			try{
				cur = ji.next();
			}
			catch(UsingIteratorPastEndException e) //if at the end of the array
			{
				break;
			}
			
			if(cur >= prev)
			{
				size++;
			}
			else //look at new string on numbers
			{
				if(size > max)	//if the last string size is the new max
					max = size;
				size = 1;	//resets size. goes to 1 because we include cur as new prev
			}
			prev = cur;
		}
		if(max == 0 || size > max)
			max = size;
		return max;
		
	}
	
}
