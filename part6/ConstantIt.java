
public class ConstantIt implements SeqIt {
	protected int index = -1;
	protected int num;
	protected int value;
	ConstantIt( Constant s)
	{
		this.num = s.num;
		this.value = s.value;
	}
	public boolean hasNext()
	{
		if(index+1 < num)
			return true;
		else
			return false;
	}
	public int next() throws UsingIteratorPastEndException
	{
		index++;
		if(index >= num)	//reason for num-1 is index starts at -1
			throw new UsingIteratorPastEndException("ConstantIt");
		return value;
	}

}
