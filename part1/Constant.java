

public class Constant extends Seq {

	protected int num;
	protected int value;
	Constant( int num, int value)
	{
		this.num = num;
		this.value = value;
	}
	public String toString()
	{
		if(num == 0)
			return "[ "+ num + " : 0 ]";
		else
			return "[ "+ num + " : "+ value + " ]";
	}
}
