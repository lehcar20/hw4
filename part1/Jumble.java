
public class Jumble extends Seq {

	protected int num;
	protected int[] values;
	Jumble( int [] values)
	{
		this.num = values.length;
		this.values = values.clone();//is it the same as arrayCopy?
	}
	public String toString()
	{
		String temp = "{ "+ num + " :";
		for(int x: values)
			temp += " "+ x;
		temp += " }";
		return temp;
	}
		
}
