import java.util.*;
public class Plus {

	public static Seq plus(Constant a, Constant b)
	{
		ConstantIt c = new ConstantIt(a);
		ConstantIt d = new ConstantIt(b);
		int value = 0 ,len = 0;

		try{
		while(c.hasNext() && d.hasNext())
		{
			value = c.next() + d.next();
			len ++;
		}
		}catch (UsingIteratorPastEndException e){
			
		}
		return new Constant(len,value);
	}
	public static Seq plus(Constant a, Delta b)
	{
		ConstantIt c = new ConstantIt(a);
		DeltaIt d = new DeltaIt(b);
		//bool seqconst = false;
		int start = 0, len = 0, delta = 0;
		//boolean beginning = true;
		
		try{
			
		if(c.hasNext() && d.hasNext())
		{
			start = c.next() + d.next();
			len++;
		}
		if(c.hasNext() && d.hasNext())
		{
			delta = c.next() + d.next() - start;
			len++;
		}
		
		while(c.hasNext() && d.hasNext())
		{
			c.next();
			d.next();
			len ++;
		}
		
		
		}catch (UsingIteratorPastEndException e){
			System.err.println("SeqIt called past end");
			System.exit(1);
		}
		if( delta == 0)
		{
			return new Constant(len, start);
		}
		else if(len == 1)
		{
			return new Constant(len, start);
		}
		return new Delta(len,start,delta);
	}
	public static Seq plus(Delta a, Constant b)
	{
		DeltaIt d = new DeltaIt(a);
		ConstantIt c = new ConstantIt(b);
		
		int start = 0, len = 0, delta = 0;
		//boolean beginning = true;
		
		try{
		if(c.hasNext() && d.hasNext())
		{
			start = c.next() + d.next();
			len++;
		}
		if(c.hasNext() && d.hasNext())
		{
			delta = c.next() + d.next() - start;
			len++;
		}
		while(c.hasNext() && d.hasNext())
		{
			c.next();
			d.next();
			len ++;
		}
		}catch (UsingIteratorPastEndException e){
			System.err.println("SeqIt called past end");
			System.exit(1);
		}
		if( delta == 0)
		{
			return new Constant(len, start);
		}
		else if(len == 1)
		{
			return new Constant(len, start);
		}
		return new Delta(len,start,delta);
	}
	public static Seq plus(Delta a, Delta b)
	{
		DeltaIt c = new DeltaIt(a);
		DeltaIt d = new DeltaIt(b);
		int start = 0, len = 0, delta = 0;
		//boolean beginning = true;
		
		try{
		if(c.hasNext() && d.hasNext())
		{
			start = c.next() + d.next();
			len++;
		}
		if(c.hasNext() && d.hasNext())
		{
			delta = c.next() + d.next() - start;
			len++;
		}
		while(c.hasNext() && d.hasNext())
		{
			c.next();
			d.next();
			len ++;
		}
		}catch (UsingIteratorPastEndException e){
			System.err.println("SeqIt called past end");
			System.exit(1);
		}
		if( delta == 0)
		{
			return new Constant(len, start);
		}
		else if(len == 1)
		{
			return new Constant(len, start);
		}
		return new Delta(len,start,delta);
	}
	public static Seq plus(Jumble a, Jumble b)
	{
		JumbleIt c = new JumbleIt(a);
		JumbleIt d = new JumbleIt(b);
		int len = 0, count = 0;
		int start = 0, curr = 0,delta = 0,delta2 = 0;
		boolean isDelt = false, constant = true;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
		while(c.hasNext() && d.hasNext())
		{
			if(len == 0)
			{
				start = c.next()+ d.next();
			}
			else if(len == 1)
			{
				delta = (c.next()+d.next())-start;
				if(delta == 0)
				{
					constant = true;
				}
				else
				{
					constant = false;
					isDelt = true;
				}
				curr += delta+start;
			}
			else
			{
				delta2 = (c.next() + d.next())-curr;
				if(delta2 != 0)
				{
					constant = false; 
				}
				if(delta2 != delta)
				{
					isDelt = false;
				}
					
				curr += delta2;
			}
			len++;
		}
		if(constant)
		{
			return new Constant(len, start);
		}
		if(isDelt)
		{
			return new Delta(len, start, delta);
		}
			c = new JumbleIt(a);
			d = new JumbleIt(b);
			int []result = new int[len];
			while(c.hasNext() && d.hasNext())
			{
				result[count]= c.next() + d.next();
				count++;
			}
			return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		return new Jumble(new int [1]);
	
	}
	public static Seq plus(Constant a, Jumble b)
	{
		ConstantIt c = new ConstantIt(a);
		JumbleIt d = new JumbleIt(b);
		int len = 0, count = 0;
		int start = 0, curr = 0,delta = 0,delta2 = 0;
		boolean isDelt = false, constant = true;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
		while(c.hasNext() && d.hasNext())
		{
			if(len == 0)
			{
				start = c.next()+ d.next();
			}
			else if(len == 1)
			{
				delta = (c.next()+d.next())-start;
				if(delta == 0)
				{
					constant = true;
				}
				else
				{
					constant = false;
					isDelt = true;
				}
				curr += delta+start;
			}
			else
			{
				delta2 = (c.next() + d.next())-curr;
				if(delta2 != 0)
				{
					constant = false; 
				}
				if(delta2 != delta)
				{
					isDelt = false;
				}
					
				curr += delta2;
			}
			len++;
		}
		if(constant)
		{
			return new Constant(len, start);
		}
		if(isDelt)
		{
			return new Delta(len, start, delta);
		}
			c = new ConstantIt(a);
			d = new JumbleIt(b);
			int []result = new int[len];
			while(c.hasNext() && d.hasNext())
			{
				result[count]= c.next() + d.next();
				count++;
			}
			return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		
		return new Jumble(new int [1]);
	
	}
	public static Seq plus(Jumble a, Constant b)
	{
		ConstantIt c = new ConstantIt(b);
		JumbleIt d = new JumbleIt(a);
		int len = 0, count = 0;
		int start = 0, curr = 0, delta = 0, delta2 = 0;
		boolean isDelt = false, constant = true;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
		while(c.hasNext() && d.hasNext())
		{
			if(len == 0)
			{
				start = c.next()+ d.next();
			}
			else if(len == 1)
			{
				delta = (c.next()+d.next())-start;
				if(delta == 0)
				{
					constant = true;
				}
				else
				{
					constant = false;
					isDelt = true;
				}
				curr += delta+start;
			}
			else
			{
				delta2 = (c.next() + d.next())-curr;
				if(delta2 != 0)
				{
					constant = false; 
				}
				if(delta2 != delta)
				{
					isDelt = false;
				}
					
				curr += delta2;
			}
			len++;
		}
		if(constant)
		{
			return new Constant(len, start);
		}
		if(isDelt)
		{
			return new Delta(len, start, delta);
		}
			c = new ConstantIt(b);
			d = new JumbleIt(a);
			int []result = new int[len];
			while(c.hasNext() && d.hasNext())
			{
				result[count]= c.next() + d.next();
				count++;
			}
			return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		return new Jumble(new int [1]);
		
	}
	public static Seq plus(Delta a, Jumble b)
	{
		DeltaIt c = new DeltaIt(a);
		JumbleIt d = new JumbleIt(b);
		int len = 0, count = 0;
		int start = 0, curr = 0,delta = 0,delta2 = 0;
		boolean isDelt = false, constant = true;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
		while(c.hasNext() && d.hasNext())
		{
			if(len == 0)
			{
				start = c.next()+ d.next();
			}
			else if(len == 1)
			{
				delta = (c.next()+d.next())-start;
				if(delta == 0)
				{
					constant = true;
				}
				else
				{
					constant = false;
					isDelt = true;
				}
				curr += delta+start;
			}
			else
			{
				delta2 = (c.next() + d.next())-curr;
				if(delta2 != 0)
				{
					constant = false; 
				}
				if(delta2 != delta)
				{
					isDelt = false;
				}
					
				curr += delta2;
			}
			len++;
		}
		if(constant)
		{
			return new Constant(len, start);
		}
		if(isDelt)
		{
			return new Delta(len, start, delta);
		}
			c = new DeltaIt(a);
			d = new JumbleIt(b);
			int []result = new int[len];
			while(c.hasNext() && d.hasNext())
			{
				result[count]= c.next() + d.next();
				count++;
			}
			return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		return new Jumble(new int [1]);
	}
	public static Seq plus(Jumble a, Delta b)
	{
		DeltaIt c = new DeltaIt(b);
		JumbleIt d = new JumbleIt(a);
		int len = 0, count = 0;
		int start = 0, curr = 0,delta = 0,delta2 = 0;
		boolean isDelt = false, constant = true;
		//Integer value = 0;
		//ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
		while(c.hasNext() && d.hasNext())
		{
			if(len == 0)
			{
				start = c.next()+ d.next();
			}
			else if(len == 1)
			{
				delta = (c.next()+d.next())-start;
				if(delta == 0)
				{
					constant = true;
				}
				else
				{
					constant = false;
					isDelt = true;
				}
				curr += delta+start;
			}
			else
			{
				delta2 = (c.next() + d.next())-curr;
				if(delta2 != 0)
				{
					constant = false; 
				}
				if(delta2 != delta)
				{
					isDelt = false;
				}
					
				curr += delta2;
			}
			len++;
		}
		if(constant)
		{
			return new Constant(len, start);
		}
		if(isDelt)
		{
			return new Delta(len, start, delta);
		}
		c = new DeltaIt(b);
		d = new JumbleIt(a);
		int []result = new int[len];
		while(c.hasNext() && d.hasNext())
		{
			result[count]= c.next() + d.next();
			count++;
		}
		return new Jumble(result);
		} catch (UsingIteratorPastEndException e){
			System.err.println("JumbleIt called past end");
			System.exit(1);
		}
		
		return new Jumble(new int [1]);
	
	}
}
