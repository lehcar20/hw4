
public class Delta extends Seq {

	protected int num;
	protected int initial;
	protected int delta;
	Delta( int num, int initial, int delta)
	{
		this.num = num;
		this.initial = initial;
		this.delta = delta;
	}
	public int min()
	{
		if(num == 0)
			return 0;
		if(delta > 0)
			return initial;
		return (initial+(num-1)*delta);
	}
	public String toString()
	{
		if(num == 0)
			return "< "+ num + " : 0 &0 >";
		else
			return "< "+ num + " : "+ initial +" &"+ delta + " >";
	}
}
