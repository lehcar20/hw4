
public class JumbleUser {
	protected static JumbleIt ji;
	
		public static int lengthLongestNDCSS1(Jumble j)
		{
			ji = new JumbleIt(j);
			int prev;
			int size = 0;
			int max = 0;
			int cur;
			if(ji.hasNext())	//j5:{ 5 : 102 103 101 107 109 }:
			{
				size++;
				prev = ji.next();
			}
			else
				return max;	//if it returns here size = 0 and max = 0
			
			while(ji.hasNext())
			{
				cur = ji.next();
				if(cur >= prev)
				{
					size++;
				}
				else
				{
					if(size > max)	//if the new size is the new max
						max = size;
					size = 1;	//resets size. goes to 1 because we include cur as new prev
				}
				prev = cur;
			}
			
			if(max == 0 || size > max)	//if there is only one string of NDCSS
				max = size;
			return max;
		}
}
