
public class Jumble extends Seq {

	protected int num;
	protected int[] values;
	Jumble( int [] values)
	{
		this.num = values.length;
		this.values = values.clone();//is it the same as arrayCopy?
	}
	public int min()
	{
		if(num == 0)
			return 0;
		int min = values[0];
		for(int x: values){
			if(x < min)
				min = x;
		}
		return min;	
	}
	public String toString()
	{
		String temp = "{ "+ num + " :";
		for(int x: values)
			temp += " "+ x;
		temp += " }";
		return temp;
	}
		
}
