
public class DeltaIt implements SeqIt {
	protected int index = -1;
	protected int num;
	protected int value;
	protected int delta;
	DeltaIt( Delta s)
	{
		this.num = s.num;
		this.value = s.initial;
		this.delta = s.delta;
	}
	public boolean hasNext()
	{
		if(index+1 < num)
			return true;
		else
			return false;
	}
	public int next()
	{
		index++;
		if(index >= num)	//reason for num-1 is index starts at -1
		{
			System.err.println("DeltaIt called past end");
			System.exit(1);
		}
		return (value + index*delta);
	}
}
