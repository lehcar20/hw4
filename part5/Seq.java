// the Seq base class

public abstract class Seq {
    public abstract SeqIt createSeqIt();
    public abstract int min();
}
