

public class Constant extends Seq {

	protected int num;
	protected int value;
	Constant( int num, int value)
	{
		this.num = num;
		this.value = value;
	}
	public SeqIt createSeqIt()
	{
		return new ConstantIt(this);
	}
	public int min()
	{
		if(num == 0)
			return 0;
		return value;	
	}
	public String toString()
	{
		if(num == 0)
			return "[ "+ num + " : 0 ]";
		else
			return "[ "+ num + " : "+ value + " ]";
	}
}
